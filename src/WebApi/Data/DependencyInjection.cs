﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApi.Data;

namespace WebApi.Data
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection
            services, IConfiguration configuration)
        {
            services.AddDbContext<CustomerContext>(opts =>
               opts.UseNpgsql(configuration.GetConnectionString("SqlConnection"), b => b.MigrationsAssembly("WebApi")));
      
            return services;
        }
    }
}
