using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Data;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("customers")]
    public class CustomerController : Controller
    {
        private readonly CustomerContext _context;
        public CustomerController(CustomerContext context) { _context = context; }

        
        [HttpGet("{id:long}")]  
        public async Task<ActionResult<Customer>> GetCustomerAsync([FromRoute] long id)
        {
            var customer = await _context.Customers.FirstOrDefaultAsync(i => i.Id == id); 
            if (customer == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(customer);
            }
        }

        [HttpPost("")]   
        public async Task<ActionResult<long>> CreateCustomerAsync([FromBody] Customer customerDto)
        {

            try
            {
                await _context.AddAsync(customerDto);
                await _context.SaveChangesAsync();
                return Ok(customerDto.Id);
            }
            catch (Exception ex)
            {
                return Conflict(ex.Message);
            }
        }
    }
}