﻿using System;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
namespace WebClient
{
    static class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Для создания пользователя введите 'creat' \nДля получения данных по id введите 'get' \nДля выхода введите 'exit' ");
            bool start = true;
            while (start)
            {
                var cmd = Console.ReadLine();
                switch (cmd.ToLower().Trim())
                {
                    case "creat":
                        var randomCustomer = RandomCustomer();
                        var id = await CreatCustomerAsync(randomCustomer);
                        var customer = await GetCustomerAsync(id);
                        Console.WriteLine(customer.ToString());
                        break;
                    case "get":
                        Console.WriteLine("Введите id");
                        customer = await GetCustomerAsync(int.Parse(Console.ReadLine()));
                        Console.WriteLine(customer?.ToString());
                        break;
                    case "exit":
                        start = false;
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Проверьте введеную команду");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;

                }
            }
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            return new CustomerCreateRequest();
        }

        private static async Task<Customer> GetCustomerAsync(long id)
        {
            string url = $"customers/{id}";
            var response = await Configurations.client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadFromJsonAsync<Customer>();
                return result;
            }
            else
            {
                Console.WriteLine("Код ошибки: {0}", response.StatusCode);
                return null;
            }
        }

        private static async Task<long> CreatCustomerAsync(CustomerCreateRequest customer)
        {
            string url = $"customers/";
            using var response = await Configurations.client.PostAsJsonAsync (url, customer);

            if (response.IsSuccessStatusCode)
            {
                var result = await response.Content.ReadAsStringAsync();
                return JsonSerializer.Deserialize<long>(result);
            }
            else
            {
                Console.WriteLine("Код ошибки: {0}", response.StatusCode);
                return 0;
            }
        }
    }
}