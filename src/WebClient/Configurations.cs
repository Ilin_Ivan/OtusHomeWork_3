﻿using System;
using System.Net.Http;

namespace WebClient
{
    public static class Configurations
    {
        public static readonly HttpClient client = new() { BaseAddress = new Uri("https://localhost:5001/") };
    }
}
