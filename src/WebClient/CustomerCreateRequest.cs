using RandomNameGeneratorLibrary;

namespace WebClient
{
    public class CustomerCreateRequest
    {
        
        public string Firstname { get; init; }
        public string Lastname { get; init; }
        public CustomerCreateRequest()
        {
            var personGenerator = new PersonNameGenerator();
            Firstname = personGenerator.GenerateRandomFirstName();
            Lastname = personGenerator.GenerateRandomLastName();
        }


    }
}